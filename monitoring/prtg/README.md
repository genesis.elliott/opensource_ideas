# proccheck

A simple tool to monitor the state of a service, that can be used with PRTG SSH Script Sensor.

# License

This program is under the BSD 3-Clause License, please see the LICENSE file.

# Prerequisite

- Operating System - Linux, tested on Ubuntu and CentOS.
- Google Go - Requires version at least 1.10.1 or later, with GOPATH configured.  [See Install Instruction Here](https://golang.org/doc/install).

# Compiling

Download the ***github.com/shirou/gopsutil/process*** library.

    go get "github.com/shirou/gopsutil"

Download ***proccheck.go*** from https://gitlab.com/genesis.elliott/opensource_ideas/tree/master/monitoring/prtg.  Compile like so:

    go build proccheck.go

This will create our executable named ***proccheck***.

# Installation

On our target server, create the directory ***/var/prtg/scripts***.

    mkdir -p /var/prtg/scripts

Copy ***proccheck*** binary file to our server.

    scp proccheck.go root@prod-jenkins.corp.local:/var/prtg/scripts

On our target server, create a wrapper script for proccheck.  For example, we want to monitor that our jenkins service is running.

    vim /var/prtg/scripts/check_jenkins_deamon.sh

      #!/bin/bash
      /var/prtg/scripts/proccheck --desc 'Jenkins Server' -pspattern jenkins\.war

Make sure, our wrapper script is executable.

    chmod +x /var/prtg/scripts/check_jenkins_deamon.sh

Perform a test, make sure our wrapper and proccheck is able to pick up our service.

    /var/prtg/scripts/check_jenkins_deamon.sh
      
      0:100:Jenkins Server is running

    echo $?
    
      0

# Command Line Arguments

proccheck has two mandatory command line arguments.

- -desc - Description of the service, will be added to the returned message.
- -pspattern - Regular expression to use when looking for a process in the process list.

# Configuring Our Target Server for Monitoring from PRTG

Use the following link to configure service monitoring on a Linux server via ssh, https://www.paessler.com/manuals/prtg/ssh_script_sensor.

# Output

If the service is running, proccheck will output the following:

    0:100:<Description that was passed> is running

And proccheck will exit with 0 exit code.

However if the service is NOT running, the output is:

    4:0:<Description that was passed> is NOT running!

And will exit with exit code of 4.