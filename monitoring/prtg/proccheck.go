// Copyright 2018 Genesis Elliott <genesis DOT elliott AT gmail DOT com>. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Tells prtg whether a service is running or not.
//
// Sample Usage:
// ./proccheck  -desc "Network Manager" -pspattern networkd
//		0:100:Network Manager is running
//
// ./proccheck  -desc "Network Manager" -pspattern networkda
//		4:0:Network Manager is NOT running!

package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/shirou/gopsutil/process"
)

var (
	desc      string
	pspattern string
	version   string
)

func main() {
	version = "0.1"

	// Define our command line arguments, and defaults
	flag.StringVar(&desc, "desc", "", "Description of the service, will be added to the returned message")
	flag.StringVar(&pspattern, "pspattern", "", "Regular expression to use when looking for a process in the process list")

	// Parse our arguments
	flag.Parse()

	// Let's trim the empty spaces on our arguments
	desc = strings.TrimSpace(desc)
	pspattern = strings.TrimSpace(pspattern)

	// If our desc or pspattern is empty, then display our usage
	// and exit with error code
	if len(desc) == 0 || len(pspattern) == 0 {
		usage()
		os.Exit(1)
	}

	// Let's get the list of processes currently running on our system
	procs, _ := process.Processes()

	// Turn our pspattern argument to a regular expression so we can use it to
	// filter our process list for our target process
	rgx, err := regexp.Compile(pspattern)
	if err != nil {
		log.Fatal(err)
	}

	// Get the name of this executable, we'll use it to create a regular expression
	// So that we can exclude it when looking for our target process, otherwise it will
	// always show up and our program will always output that what we're looking for
	// is running, when actually it's just this program
	executable, _ := os.Executable()
	rgxExecutable, _ := regexp.Compile(filepath.Base(executable))

	found := false

	for _, proc := range procs {
		// Let's extract the complete command line argument for each process
		args, _ := proc.Cmdline()

		// If args matches our regular expression, and does not this match this program
		// set found to true
		if rgx.MatchString(args) && !rgxExecutable.MatchString(args) {
			found = true
			break
		}
	}

	if found {
		// Our target process is running
		fmt.Printf("0:100:%s is running\n", desc)
		os.Exit(0)
	} else {
		// Our target process is not running
		fmt.Printf("4:0:%s is NOT running!\n", desc)
		os.Exit(4)
	}
}

// usage Print the usage of this program
func usage() {
	fmt.Printf("proccheck v%s\n", version)

	flag.PrintDefaults()
}
